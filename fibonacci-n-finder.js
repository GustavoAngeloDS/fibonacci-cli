class FibonacciNFinder {
    sequenceNumber = null;

    constructor(n) {
        this.sequenceNumber = n;
    }

    calculateFibonacci() {
        let currentFibonacciNumber = 0;
        let lastFibonacciNumber = 0;

        let fibonacciNumberValue = null;
        for(let i = 0; i < this.sequenceNumber; i++) {
            fibonacciNumberValue = currentFibonacciNumber;
            
            currentFibonacciNumber = currentFibonacciNumber + lastFibonacciNumber;
            
            lastFibonacciNumber = currentFibonacciNumber - lastFibonacciNumber;

            if(currentFibonacciNumber == 0)
                currentFibonacciNumber++;
        }

        return (fibonacciNumberValue);
    }
}

module.exports = FibonacciNFinder;