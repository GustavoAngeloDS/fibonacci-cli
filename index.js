#!/usr/bin/env node

const yargs = require('yargs');
const FibonacciNFinder = require('./fibonacci-n-finder.js');


module.exports = () => {
  const options = yargs
   .usage("Usage: -n <nº-number>")
   .option("n", { alias: "nº fibonacci sequence number", describe: "Sequence number to find", type: "string", demandOption: true })
   .argv;

  const fib = new FibonacciNFinder(options.n);
    
  console.log(fib.calculateFibonacci());
}
