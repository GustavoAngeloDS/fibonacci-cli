import FibonacciNFinder from "./fibonacci-n-finder.js"

describe('Fibonacci N Finder', () => {
    describe('Units', () => {
      test('the 5º number should return 3', () => {
        const fib = new FibonacciNFinder(5);
        expect(fib.calculateFibonacci()).toEqual(3);
      });
      
      test('the 10º number should return 34', () => {
        const fib = new FibonacciNFinder(10);
        expect(fib.calculateFibonacci()).toEqual(34);
      });

      test('the 1º number should return 0', () => {
        const fib = new FibonacciNFinder(1);
        expect(fib.calculateFibonacci()).toEqual(0);
      });
    });
  });
  